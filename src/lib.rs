use std::collections::HashMap;
use std::io;
use std::os::unix::io::RawFd;
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, Mutex, RwLock};

use once_cell::sync::Lazy;
use virtio_bindings::bindings::virtio_net::*;
use virtio_bindings::bindings::virtio_ring::*;
use virtio_queue::{DescriptorChain, Queue};
use vm_memory::{
    Address, GuestAddress, GuestAddressSpace, GuestMemory, GuestMemoryAtomic, GuestMemoryLoadGuard,
    GuestMemoryMmap, GuestMemoryRegion, GuestRegionMmap, MmapRegion,
};

use virtiofsd::descriptor_utils::{Reader, Writer};
use virtiofsd::fs_cache_req_handler_dummy::FsCacheReqHandler;
use virtiofsd::fuse;
use virtiofsd::passthrough::{Config, PassthroughFs};
use virtiofsd::server::Server;

mod vhost_user;
use vhost_user::*;

#[derive(Clone)]
struct DummySlaveFsCacheReq {}

impl FsCacheReqHandler for DummySlaveFsCacheReq {
    fn map(
        &mut self,
        foffset: u64,
        moffset: u64,
        len: u64,
        flags: u64,
        fd: RawFd,
    ) -> io::Result<()> {
        Ok(())
    }

    fn unmap(&mut self, requests: Vec<fuse::RemovemappingOne>) -> io::Result<()> {
        Ok(())
    }
}

struct Backend {
    atomic_mem: GuestMemoryAtomic<GuestMemoryMmap>,
    server: Arc<Server<PassthroughFs>>,
    queues: Vec<Queue<GuestMemoryAtomic<GuestMemoryMmap>>>,
    vu_req: Option<DummySlaveFsCacheReq>,
}

impl Backend {
    fn new() -> Result<Self, std::io::Error> {
        Ok(Backend {
            atomic_mem: GuestMemoryAtomic::new(GuestMemoryMmap::new()),
            server: Arc::new(Server::new(PassthroughFs::new(Config::default()).unwrap())),
            queues: Vec::new(),
            vu_req: None,
        })
    }

    pub fn vmm_va_to_gpa(&self, vmm_va: u64) -> u64 {
        println!("XXX - vmm_va_to_gpa");
        let mem = self.atomic_mem.memory();
        for region in mem.iter() {
            println!("region addr: {:x}", region.as_ptr() as u64);
            println!("region size: {}", region.size());
            let region_start = region.as_ptr() as u64;
            if vmm_va >= region_start && vmm_va < region_start + region.size() as u64 {
                println!("vmm_va_to_gpa: found region");
                let gpa = vmm_va - region_start + region.start_addr().raw_value() as u64;
                let va = mem.get_host_address(GuestAddress(gpa)).unwrap() as u64;
                println!("vmm_va_to_gpa: {:x} to {:x} to {:x}", vmm_va, gpa, va);
                return gpa;
            }
        }

        panic!("vmm_va_to_gpa: not found");
    }

    pub fn process_queue(&mut self, idx: u32) {
        println!("XXX - process_queue {}", idx);
        let queue = &mut self.queues[idx as usize];
        let mem = self.atomic_mem.memory();

        println!("XXX - next avail {}", queue.next_avail());
        println!("XXX - size {}", queue.state.size);
        let avail_chains: Vec<DescriptorChain<GuestMemoryLoadGuard<GuestMemoryMmap>>> =
            queue.iter().unwrap().collect();

        for chain in avail_chains {
            println!("XXX - something in queue {}", idx);

            let head_index = chain.head_index();
            println!("XXX - head_index: {}", head_index);

            let reader = Reader::new(&mem, chain.clone()).unwrap();
            let writer = Writer::new(&mem, chain.clone()).unwrap();

            if let Err(e) = self
                .server
                .handle_message(reader, writer, self.vu_req.as_mut())
            {
                println!("Error processing message {}", e);
            }

            if let Err(e) = queue.add_used(head_index, 0) {
                panic!("Couldn't return used descriptors to the ring: {}", e);
            }
        }
    }
}

static BACKEND_IDS: AtomicU32 = AtomicU32::new(0);
static BACKEND_MAP: Lazy<RwLock<HashMap<u32, Arc<Mutex<Backend>>>>> =
    Lazy::new(|| RwLock::new(HashMap::new()));

#[no_mangle]
pub extern "C" fn virtiofsd_backend_init() -> i32 {
    let backend_id = BACKEND_IDS.fetch_add(1, Ordering::SeqCst);
    BACKEND_MAP.write().unwrap().insert(
        backend_id as u32,
        Arc::new(Mutex::new(Backend::new().unwrap())),
    );
    return backend_id as i32;
}

#[no_mangle]
pub extern "C" fn virtiofsd_get_features() -> u64 {
    println!("get_features");
    1 << VIRTIO_F_VERSION_1
        | 1 << VIRTIO_RING_F_INDIRECT_DESC
        | 1 << VIRTIO_RING_F_EVENT_IDX
        | VhostUserVirtioFeatures::PROTOCOL_FEATURES.bits()
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_features(features: u64) -> i32 {
    println!("set_features");
    if features & !virtiofsd_get_features() != 0 {
        return -22;
    }

    return 0;
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_vring_call(_idx: i32, _fd: RawFd) -> i32 {
    println!("set_vring_call");
    return 0;
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_vring_kick(_idx: i32, _fd: RawFd) -> i32 {
    println!("set_vring_kick");
    return 0;
}

#[repr(C)]
#[derive(Debug)]
pub struct VhostMemoryRegion {
    guest_phys_addr: u64,
    memory_size: u64,
    userspace_addr: u64,
    flags_padding: u64,
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_mem_table(c_regions: *mut VhostMemoryRegion, nregions: i32) -> i32 {
    println!("set_mem_table");
    let regions: &mut [VhostMemoryRegion] =
        unsafe { std::slice::from_raw_parts_mut(c_regions, nregions as usize) };
    for (i, region) in regions.iter().enumerate() {
        println!("region {}: {:?}", i, region);
        println!("region {}: userspace={:x}", i, region.userspace_addr);
    }

    let mut mmap_regions: Vec<GuestRegionMmap> = Vec::new();
    for region in regions {
        let mr = unsafe {
            MmapRegion::build_raw(
                region.userspace_addr as *mut u8,
                region.memory_size as usize,
                0,
                0,
            )
            .unwrap()
        };

        mmap_regions.push(GuestRegionMmap::new(mr, GuestAddress(region.guest_phys_addr)).unwrap());
    }

    let mem = GuestMemoryMmap::from_regions(mmap_regions).unwrap();
    let backend = BACKEND_MAP.read().unwrap().get(&0).unwrap().clone();
    backend
        .lock()
        .unwrap()
        .atomic_mem
        .lock()
        .unwrap()
        .replace(mem);

    return 0;
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_vring_num(idx: u32, num: u32) -> i32 {
    println!("set_vring_num");
    println!("TODO: set vring {} size to {}", idx, num);
    return 0;
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_vring_base(idx: u32, num: u32) -> i32 {
    println!("set_vring_base");
    println!("TODO: set vring {} base to {}", idx, num);
    return 0;
}

#[repr(C)]
pub struct VhostRingAddr {
    index: u32,
    /* Option flags. */
    flags: u32,

    /* Start of array of descriptors (virtually contiguous) */
    desc_user_addr: u64,
    /* Used structure address. Must be 32 bit aligned */
    used_user_addr: u64,
    /* Available structure address. Must be 16 bit aligned */
    avail_user_addr: u64,
    /* Logging support. */
    /* Log writes to used structure, at offset calculated from specified
     * address. Address must be 32 bit aligned. */
    log_guest_addr: u64,
}

#[no_mangle]
pub extern "C" fn virtiofsd_set_vring_addr(c_addr: *mut VhostRingAddr) -> i32 {
    println!("set_vring_addr");
    let addr: &VhostRingAddr = unsafe { &std::slice::from_raw_parts_mut(c_addr, 1)[0] };
    println!(
        "set vring {} desc_addr to {:x}",
        addr.index, addr.desc_user_addr
    );
    println!(
        "set vring {} used_addr to {:x}",
        addr.index, addr.used_user_addr
    );
    println!(
        "set vring {} avail_addr to {:x}",
        addr.index, addr.avail_user_addr
    );

    let backend_lock = BACKEND_MAP.read().unwrap().get(&0).unwrap().clone();
    let mut backend = backend_lock.lock().unwrap();

    let idx = addr.index as usize;
    let mut queue: Queue<GuestMemoryAtomic<GuestMemoryMmap>> =
        Queue::new(backend.atomic_mem.clone(), 128);

    queue.state.desc_table = GuestAddress(backend.vmm_va_to_gpa(addr.desc_user_addr));
    queue.state.used_ring = GuestAddress(backend.vmm_va_to_gpa(addr.used_user_addr));
    queue.state.avail_ring = GuestAddress(backend.vmm_va_to_gpa(addr.avail_user_addr));
    queue.set_ready(true);

    backend.queues.insert(idx, queue);

    println!("set_vring_addr: queues: {}", backend.queues.len());

    return 0;
}

#[no_mangle]
pub extern "C" fn virtiofsd_handle_output() -> i32 {
    println!("handle_output");

    let backend_lock = BACKEND_MAP.read().unwrap().get(&0).unwrap().clone();
    let mut backend = backend_lock.lock().unwrap();

    backend.process_queue(0);
    backend.process_queue(1);

    return 1;
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
